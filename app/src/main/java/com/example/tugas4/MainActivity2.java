package com.example.tugas4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity2 extends AppCompatActivity implements View.OnClickListener{
    TextView angka;
    TextView hasilkalkulator;
    Button btBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        angka = findViewById(R.id.hasil);
        hasilkalkulator = findViewById(R.id.operasi);
        btBack = findViewById(R.id.btBack);

        btBack.setOnClickListener(this);

        Intent intent = getIntent();
        String hasil = intent.getStringExtra("Hasil");
        String operasi = intent.getStringExtra("Operasi");

        angka.setText(hasil);
        hasilkalkulator.setText(operasi);
    }
    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btBack) {
            finish();
        }
    }
}